function matriz_mult(matriz1, matriz2){
    var num_linhas_m1 = matriz1.length, num_colunas_m1 = matriz1[0].length,
        num_linhas_m2 = matriz2.length, num_colunas_m2 = matriz2[0].length,
        m_final = new Array(num_linhas_m1);
    for (var r = 0; r < num_linhas_m1; ++r) {
        m_final[r] = new Array(num_colunas_m2);
        for (var c = 0; c < num_colunas_m2; ++c) {
            m_final[r][c] = 0;
            for (var i = 0; i < num_colunas_m1; ++i) {
                m_final[r][c] += matriz1[r][i] * matriz2[i][c];
            }
        }
    }
    return m_final;
}
console.log(matriz_mult( [ [ [2],[-1] ], [ [2],[0] ] ],[ [2,3],[-2,1] ] ));
console.log(matriz_mult([[4,0],[-1,-1]], [[-1,3],[2,7]] ));